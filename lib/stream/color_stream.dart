import 'package:flutter/material.dart';

class VocabStream {
  // Stream colorStream;

  Stream<String> getVocab() async* {
    final List<String> vocab = [
      'classico', 'hygiene', 'techniques', 'fistful', 'philanthropy', 'majors',
      'coho', 'tags', 'proceeded', 'quarter', 'massa', 'snipsnap', 'remarkably',
      'baseman', 'slang', 'cinque', 'irreverent', 'vacances', 'continua',
      'organisms', 'mana', 'flet', 'homemaker', 'irma', 'forma', 'heartburn',
      'klan', 'stockbridge', 'lompoc', 'magix', 'dyeing', 'cdx', 'loans',
      'undergraduates', 'lattice', 'webster', 'raping', 'techie', 'talents',
      'yay', 'nocd', 'coon', 'pcc', 'bonded', 'chevalier', 'pauses', 'frugal',
      'bodies', 'miquelon', 'dunst', 'rafts', 'maxx', 'roaming', 'effie',
      'calculated', 'kelli', 'pains', 'multicolor', 'augmented', 'dren',
      'changers', 'pfizer', 'waterville', 'behav', 'memes', 'compendium',
      'mums', 'microelectronics', 'prag', 'ambulances', 'yangon', 'nowak',
      'phonetics', 'cameroon', 'detainee', 'eason', 'antipsychotic', 'fwy',
      'adenosine', 'descended', 'tick', 'goodmans', 'unintentional', 'salazar',
      'aliso', 'progressing', 'gutter', 'foxtrot', 'lamont', 'precursor',
      'mulholland', 'delectable', 'wearer', 'whereupon', 'bravely', 'bowed',
      'marries', 'nuevo', 'mobile', 'roebuck'
    ];

    yield* Stream.periodic(const Duration(seconds: 1), (int t) {
      int index = t;
      return vocab[index];
    });
  }
}